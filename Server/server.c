#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define square(arg) (arg) * (arg)

/* Type to store coordinates */
typedef struct coord
{
	double longitude;
	double latitude;
} Coord;

int make_socket(uint16_t);				// initialize a socket

int within_range(Coord*);				// check if coord is within 1000m from CSIE Building

void* connection_handler(void*);		// thread function

void pi_command_handler(int);			// send commands to Pi

void android_coordinates_handler(int);	// receive coordinates from Android

void android_notification_handler(int);	// send notifications to Android

void android_command_handler(int);		// receive commands from Android

/* Globally visible and modifiable state of the AC */
int SWITCH = 0;	// 0: OFF, 1: ON
int TEMPERATURE = 22;

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "usage: %s <port number>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	int listen_fd, connect_fd;
	struct sockaddr_in cli_addr;
	int cli_len = sizeof(cli_addr);
	pthread_t tid;

	listen_fd = make_socket(atoi(argv[1]));

	while (1)
	{
		connect_fd = accept(listen_fd, (struct sockaddr*)&cli_addr, &cli_len);
	//	fprintf(stderr, "testtesttest\n");
		if (connect_fd < 0)
			handle_error("accept error");

		/* create a thread to handle the incoming connection */
		if (pthread_create(&tid, NULL, connection_handler, &connect_fd) != 0)
		{
			fprintf(stderr, "can't create thread\n");
			exit(EXIT_FAILURE);
		}
		/* do not wait for the thread to return */
		if (pthread_detach(tid) != 0)
		{
			fprintf(stderr, "can't detach thread\n");
			exit(EXIT_FAILURE);
		}
	}

	return 0;
}

int make_socket(uint16_t port)
{
	struct sockaddr_in serv_addr;
	int listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_fd < 0)
		handle_error("socket error");

	bzero((char*)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(port);

	if (bind(listen_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
		handle_error("bind error");

	if (listen(listen_fd, 1) < 0)
		handle_error("listen error");

	return listen_fd;
}

int within_range(Coord* coord)
{
	/* CSIE Building R102: (25.0193, 121.5416) */
	return (square(100000 * (coord->longitude - 121.5416)) + 
			 square(110000 * (coord->latitude - 25.0193)) <= square(1000));
}

void* connection_handler(void* arg)
{
	int connect_fd = *(int*)arg;
	char buf[5];

	/* read thread id, format: "<thread_id>" */
	int n = read(connect_fd, buf, 5);
	if (n < 0)
		handle_error("read error");
	if (n == 0)
	{
		close(connect_fd);
		return;
	}

	/* identify the thread */
	switch (buf[0])
	{
	 	case '0':
			printf("connection established with Pi.\n");
			/* send commands to Pi */
			pi_command_handler(connect_fd);
			break;

		case '1':
			printf("connection established with Android(receive coordinates).\n", buf[0]);
			/* receive coordinates from Android */
			android_coordinates_handler(connect_fd);
			break;

		case '2':
			printf("connection established with Android(send notifications).\n", buf[0]);
			/* send notifications to Android */
			android_notification_handler(connect_fd);
			break;

		case '3':
			printf("connection established with Android(receive commands).\n", buf[0]);
			/* receive commands from Android */
			android_command_handler(connect_fd);
			break;

		default:
			close(connect_fd);
	}

	return;
}

void pi_command_handler(int connect_fd)
{
	int n;
	char command[5];
	char buf[256];
	while (1)
	{
		if (recv(connect_fd, buf, sizeof(buf), MSG_PEEK | MSG_DONTWAIT) == 0)
			break;

		/* format: "<SWITCH> <TEMPERATURE>" */
		sprintf(command, "%d %d\n", SWITCH, TEMPERATURE);
		if ((n = write(connect_fd, command, strlen(command))) < 0)
			handle_error("error sending data to Pi");
		sleep(2);
	}
	close(connect_fd);

	return;
}

void android_coordinates_handler(int connect_fd)
{
	int n;
	char buf[30];
	int count_in = 0;
	int count_out = 0;
	while (1)
	{
		/* format: "<longitude> <latitude>" */
		if ((n = read(connect_fd, buf, 30)) < 0)
			handle_error("error receiving coordinate from Android");
		if (n == 0)
			break;
		buf[n] = '\0';
		Coord coord;
		sscanf(buf, "%lf %lf", &coord.longitude, &coord.latitude);
		
		if (within_range(&coord))
		{
			count_out = 0;
			count_in++;
		}
		else
		{
			count_in = 0;
			count_out++;
		}
		if (count_in == 3)
		{
			SWITCH = 1;
			count_in = 0;
		}
		else if (count_out == 3)
		{
			SWITCH = 0;
			count_out = 0;
		}
	}
	close(connect_fd);

	return;
}

void android_notification_handler(int connect_fd)
{
	int n;
	char notification[5];
	char buf[256];
	while (1)
	{
		if (recv(connect_fd, buf, sizeof(buf), MSG_PEEK | MSG_DONTWAIT) == 0)
			break;
		
		/* format: "<SWITCH>" */
		sprintf(notification, "%d", SWITCH);
		if ((n = write(connect_fd, notification, 1)) < 0)
			handle_error("error sending notification to Android");
		sleep(2);
	}
	close(connect_fd);

	return;
}

void android_command_handler(int connect_fd)
{
	int n;
	char buf[10];
	while (1)
	{
		/* format_0: "0 <SWITCH>" */
		/* format_1: "1 <TEMPERATURE>" */
		if ((n = read(connect_fd, buf, 10)) < 0)
			handle_error("error receiving command from Android");
		if (n == 0)
			break;
		buf[n] = '\0';

		if (buf[0] == '0')
		{
			SWITCH = atoi(buf + 2);
			if (SWITCH == 0)
				printf("Android requests to turn off the AC.\n");
			else
				printf("Android requests to turn on the AC.\n");
		}
		else if (buf[0] == '1')
		{
			printf("Android requests to set the temperature to %d.\n", atoi(buf + 2));
			TEMPERATURE = atoi(buf + 2);
		}
	}
	close(connect_fd);

	return;
}
