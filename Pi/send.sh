echo $# $0 $1

if [ $# -eq 0 ]
then
	echo "parameter?"
	exit 1
fi

irsend SEND_ONCE air $1

exit
