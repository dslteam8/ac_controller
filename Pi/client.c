#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define ERR_EXIT(a) {perror(a); exit(1);}


int IR_Control(char* para)
{
	int pid=0;
	pid = fork();
	if(pid == 0){
		if(execlp("./send.sh","./send.sh",para,NULL) < 0)
			perror("exec");
	}
	return pid;
}

int main(int argc, char**argv)//argv: .o <addr> <port>
{
	int sockfd,n,port=0;
	int power=0,temp=25,pid=0,state,state_temp=0;
	struct sockaddr_in servaddr,cliaddr;
	char addr[sizeof(struct in_addr)];
	char buf[1000];
	char sendline[1000];
	char recvline[1000];
	char para[30];

	if (argc != 3)
	{
		printf("usage:  client <IP address>\n");
		exit(1);
	}

	pid = IR_Control("power-down");
	state = 0;// air conditioner is down

	sockfd=socket(AF_INET,SOCK_STREAM,0);

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr=inet_addr(argv[1]);
	servaddr.sin_port=htons(atoi(argv[2]));
	if(connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)))
		ERR_EXIT("connect");
	

	if(write(sockfd,"0",1) != 1)//tell server i am pi 0:ispi
		ERR_EXIT("write");
	while ( (n = read(sockfd,recvline,1000))!=0 )
	{
		recvline[n]=0;
		sscanf(recvline,"%d %d",&power,&temp);
		if(fputs(recvline,stderr) <0 )
			ERR_EXIT("fputs");

		if(!power && !state)
			continue;
		else if(!power){
			pid = IR_Control("power-down");
			state = 0;
		}
		else if (!state || temp != state_temp){
			sprintf(para,"up-%d",temp);
			pid = IR_Control(para);
			state = 1;
			state_temp = temp;
		}
//		if(fgets(sendline, 1000,stdin) == NULL)
//			ERR_EXIT("fgets");
//		if(write(sockfd,sendline,strlen(sendline)) != strlen(sendline))
//			ERR_EXIT("write");
//		fprintf(stderr,"pe la\n");
//	n = read(sockfd,recvline,10000);
	}
	ERR_EXIT("read");
	return 0;
}
