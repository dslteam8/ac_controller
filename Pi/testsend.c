#include<stdlib.h>
#include<stdio.h>
#include<lirc/lirc_client.h>
#include<time.h>
#include<unistd.h>

int main()
{
	int pid;
	int ppid;
	pid = fork();
	if(pid==0){
		if(execlp("./send.sh","power-up",(char*) NULL)<0)
			perror("exec failed\n");
	}
	else{

		fprintf(stderr,"in parent, pid=%d\n",pid);
	}
}
