package com.mycompany.final_test;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.location.LocationListener;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends ActionBarActivity {

    int currTemp, defaultTemp;
    SharedPreferences.Editor editor;
    SharedPreferences sp_dftemp;
    boolean currPower;

    String address_name = "linux13.csie.org";
    int port = 5050;
    Socket receiver, commander;
    OutputStream receive_outputStream, command_outputStream;
    InputStream inputStream;

    LocationManager locationManager;
    LocationListener locationListener;
    double Lat, Lng;
    TextView vlat, vlng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);

        /*
         * Check First-time
         */
        final String PREFS_NAME = "MyPrefsFile";

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something

            Toast.makeText(this, "First time", Toast.LENGTH_SHORT).show();

            /*
             * Set Shared Preference: autoCtrl=ON, defaultTemp=24
             */
            SharedPreferences sp_autoctrl = this.getSharedPreferences(
                    getString(R.string.auto_control), Context.MODE_PRIVATE);
            editor = sp_autoctrl.edit();
            editor.putBoolean(getString(R.string.auto_control), true);
            editor.commit();

            defaultTemp = 24;
            SharedPreferences sp_dftemp = this.getSharedPreferences(
                    getString(R.string.default_temp), Context.MODE_PRIVATE);
            editor = sp_dftemp.edit();
            editor.putInt(getString(R.string.default_temp), defaultTemp);
            editor.commit();

            /*
             * Start Background Service
             */
            Intent intent = new Intent(this, BackgroundService.class);
            intent.addCategory(BackgroundService.TAG);
            startService(intent);

            settings.edit().putBoolean("my_first_time", false).commit();
        }

        /*
         *  Get Location
         */
//        double Lat, Lng;
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                String locationProvider = LocationManager.GPS_PROVIDER;
                Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
                Lat = lastKnownLocation.getLatitude();
                Lng = lastKnownLocation.getLongitude();
                vlat = (TextView) findViewById(R.id.text_ShowLat);
                vlng = (TextView) findViewById(R.id.text_ShowLng);
                vlat.setText(Double.toString(Lat));
                vlng.setText(Double.toString(Lng));


            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);


        /*
         *
         *
         * Read form server: currPower, currTemp
         */
        currPower = false;



        /*
         *  Decide what to display: OFF or the temperature
         */

        if(currPower == true){
            TextView temp = (TextView) findViewById(R.id.tv_temp);
            temp.setText(Integer.toString(defaultTemp));
        }
        else{
            TextView temp = (TextView) findViewById(R.id.tv_temp);
            temp.setText("---");
        }



        /*
         *
         */
        new Thread(ReadServer).start();
        new Thread(SendCommand).start();

    }

    Runnable ReadServer = new Runnable() {
        public void run() {
            InetAddress address = null;
            try {
                address = InetAddress.getByName(address_name);
                receiver = new Socket(address, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(receiver == null) {
                Looper.prepare();
                Toast.makeText(getApplicationContext(), "null receiver socket", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
            String id = "2";
            byte[] id_buffer = id.getBytes();
            try {
                receive_outputStream = receiver.getOutputStream();
                receive_outputStream.write(id_buffer);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            try {
                inputStream = receiver.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                public void run() {
                    try {
                        int notification = inputStream.read();
                        notification -= 48;
                        /*String not = String.format("read %d", notification);
                        Looper.prepare();
                        Toast.makeText(getApplicationContext(), not, Toast.LENGTH_SHORT).show();
                        Looper.loop();*/
                        switch(notification) {
                            case 0:
                                if(currPower == true) {
                                    currPower = false;
                                    //TextView temp = (TextView) findViewById(R.id.tv_temp);
                                    //temp.setText("---");
                                    Looper.prepare();
                                    Toast.makeText(getApplicationContext(), "AC has just turned off", Toast.LENGTH_SHORT).show();
                                    Looper.loop();
                                }
                                break;
                            case 1:
                                if(currPower == false) {
                                    currPower = true;
                                    //TextView temp = (TextView) findViewById(R.id.tv_temp);
                                    //temp.setText(Integer.toString(defaultTemp));
                                    Looper.prepare();
                                    Toast.makeText(getApplicationContext(), "AC has just turned on", Toast.LENGTH_SHORT).show();
                                    Looper.loop();
                                }
                                break;
                            default:
                                Looper.prepare();
                                Toast.makeText(getApplicationContext(), "Undefined Action", Toast.LENGTH_SHORT).show();
                                Looper.loop();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            timer.schedule(timerTask, 1000, 30000);
        }
    };

    Runnable SendCommand = new Runnable() {
        public void run() {
            InetAddress address = null;
            try {
                address = InetAddress.getByName(address_name);
                commander = new Socket(address, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(commander == null) {
                Looper.prepare();
                Toast.makeText(getApplicationContext(), "null commander socket", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
            String id = "3";
            byte[] id_buffer = id.getBytes();
            try {
                command_outputStream = commander.getOutputStream();
                command_outputStream.write(id_buffer);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            try {
                command_outputStream = commander.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    };


    /*
     * Respond to button clicks
     */
    public void up(View view){
        if(currPower==false)
            return;
        if(currTemp < 32){
            currTemp++;
            TextView temp = (TextView) findViewById(R.id.tv_temp);
            temp.setText(Integer.toString(currTemp));
        }
        Toast.makeText(getApplicationContext(), "Temp Increase", Toast.LENGTH_SHORT).show();
    }

    public void down(View view){
        if(currPower==false)
            return;
        if(currTemp > 16){
            currTemp--;
            TextView temp = (TextView) findViewById(R.id.tv_temp);
            temp.setText(Integer.toString(currTemp));
        }
        Toast.makeText(getApplicationContext(), "Temp Decrease", Toast.LENGTH_SHORT).show();
    }

    public void sendtemp(View view){
        String command = String.format("1 %d", currTemp);
        byte[] write_buffer = command.getBytes();
        try {
            command_outputStream.write(write_buffer);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void power(View view){
        int power = 0;
        if(currPower == true){
            /*
             *  Send to server: currPower=flase(or "OFF")
             */
            TextView temp = (TextView) findViewById(R.id.tv_temp);
            temp.setText("---");
            currPower = false;
            power = 0;
            Toast.makeText(getApplicationContext(), "Power OFF", Toast.LENGTH_SHORT).show();
        }
        else{
            /*
             * Send to server: currPower=true, currTemp=defaultTemp
             */
            sp_dftemp = this.getSharedPreferences(getString(R.string.default_temp), Context.MODE_PRIVATE);
            defaultTemp = sp_dftemp.getInt(getString(R.string.default_temp), 24);

            currPower = true;
            power = 1;
            currTemp = defaultTemp;
            TextView temp = (TextView) findViewById(R.id.tv_temp);
            temp.setText(Integer.toString(defaultTemp));
            Toast.makeText(getApplicationContext(), "Power ON", Toast.LENGTH_SHORT).show();

        }
        String command = String.format("0 %d", power);
        byte[] write_buffer = command.getBytes();

        try {
            command_outputStream.write(write_buffer);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Settings", Toast.LENGTH_SHORT).show();
            /*
            * send DefaulTemp, auto-control
            * */
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(receiver != null) {
            try {
                receiver.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(commander != null) {
            try {
                commander.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
