package com.mycompany.final_test;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

public class BackgroundService extends Service {

    public static final String TAG = "bgServiceTag";
    LocationManager locationManager;
    LocationListener locationListener;
    Socket sender;
    OutputStream send_outputStream;
    double Lat, Lng;
    String address_name = "linux13.csie.org";
    int port = 5050;

    public BackgroundService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
        }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        new Thread(SendLocation).start();
        /*
         *  Get Location
         */
//        double Lat, Lng;
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                String locationProvider = LocationManager.GPS_PROVIDER;
                Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
                Lat = lastKnownLocation.getLatitude();
                Lng = lastKnownLocation.getLongitude();
                Toast.makeText(getApplicationContext(), "Lat: " + Double.toString(lastKnownLocation.getLatitude()) + ", Lng: " + Double.toString(lastKnownLocation.getLongitude()), Toast.LENGTH_SHORT).show();
                /*
                 *
                 * Send to Server: Lat Lnt
                 *
                 *  */

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);


        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();

        locationManager.removeUpdates(locationListener);
        locationManager = null;
    }

    Runnable SendLocation = new Runnable() {
        public void run() {
            try {
                InetAddress address = InetAddress.getByName(address_name);
                sender = new Socket(address, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(sender == null) {
                Looper.prepare();
                Toast.makeText(getApplicationContext(), "null sender socket", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
            String id = "1";
            byte[] id_buffer = id.getBytes();
            try {
                send_outputStream = sender.getOutputStream();
                send_outputStream.write(id_buffer);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                public void run() {
                    String message = String.format("%f %f", Lng, Lat);
                    byte[] write_buffer = message.getBytes();
                    try {
                        send_outputStream.write(write_buffer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            };
            timer.schedule(timerTask, 1000, 30000);
        }
    };





    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
