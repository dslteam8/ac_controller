package com.mycompany.final_test;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;


public class Settings extends ActionBarActivity {

    Button btn_picktemp;
    NumberPicker  aNumberPicker;
    AlertDialog.Builder alertBw;
    AlertDialog alertDw;
    Boolean autoCtrl;
    int dfTemp;
    SharedPreferences sp_auto, sp_dftemp;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        /*
         * Load state
         */
        sp_auto = this.getSharedPreferences(getString(R.string.auto_control), Context.MODE_PRIVATE);
        autoCtrl = sp_auto.getBoolean(getString(R.string.auto_control), false);
        ToggleButton tgbtn =(ToggleButton) findViewById(R.id.tgbtn_auto);
        if(autoCtrl==true){
            tgbtn.setChecked(true);
        }

        sp_dftemp = this.getSharedPreferences(getString(R.string.default_temp), Context.MODE_PRIVATE);
        dfTemp = sp_dftemp.getInt(getString(R.string.default_temp), 24);
        Button btn_setdtemp =(Button) findViewById(R.id.btn_picktemp);
        btn_setdtemp.setText(Integer.toString(dfTemp));


        /*
         * Number Picker onclick
         * */
        btn_picktemp = (Button) findViewById(R.id.btn_picktemp);
        onButtonPick();
        setAlert();


    }

    public void onButtonPick (){
        btn_picktemp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                alertDw.show();
            }
        });
    }


    /*
     * NumberPicker Dialog
     * */
    public void setAlert(){

        RelativeLayout linearLayout=new RelativeLayout(this);
        aNumberPicker=new NumberPicker(this);
        aNumberPicker.setMaxValue(32);
        aNumberPicker.setMinValue(16);
        aNumberPicker.setValue(dfTemp);
        aNumberPicker.setWrapSelectorWheel(true);
        aNumberPicker.setClickable(false);
        aNumberPicker.setEnabled(true);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50,50);
        RelativeLayout.LayoutParams numPickerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        numPickerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(aNumberPicker,numPickerParams);
        linearLayout.isClickable();

        aNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });

        alertBw=new AlertDialog.Builder(this);
        alertBw.setTitle("Set Default Temperature");
        alertBw.setView(linearLayout);
        alertBw.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                int temp = aNumberPicker.getValue();
                btn_picktemp.setText(Integer.toString(temp));
                Toast.makeText(getApplicationContext(), Integer.toString(temp), Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sp_dftemp.edit();
                editor.putInt(getString(R.string.default_temp), temp);
                editor.commit();
                dialog.dismiss();
            }
        });
        alertBw.setNeutralButton("Cancel", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        alertDw=alertBw.create();
    }

    /*
     * Auto-control
     */
    public void onToggleClicked(View view) {
        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();

        if (on) {
            // Enable vibrate
            editor = sp_auto.edit();
            editor.putBoolean(getString(R.string.auto_control), true);
            editor.commit();
            Intent intent = new Intent(this, BackgroundService.class);
            intent.addCategory(BackgroundService.TAG);
            startService(intent);
        } else {
            // Disable vibrate
            editor = sp_auto.edit();
            editor.putBoolean(getString(R.string.auto_control), false);
            editor.commit();
            Intent intent = new Intent(getApplicationContext(), BackgroundService.class);
            intent.addCategory(BackgroundService.TAG);
            stopService(intent);
        }
    }




/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

}
